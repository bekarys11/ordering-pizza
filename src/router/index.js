import { createRouter, createWebHistory } from "vue-router";
import App from "../App.vue";
import Order from "../views/Order.vue";
import Menu from "../views/Menu.vue";
import Home from "../components/Carousel.vue";
import Locations from "../views/Locations.vue";
import Restaurant from "../components/Restaurant.vue";
import Cart from "../components/Cart.vue";
import MenuCardWithMoreInfo from "../components/MenuCardWithMoreInfo.vue";

const routes = [
  {
    path: "/",
    redirect: "/home",
    name: "App",
    component: App,
  },

  {
    path: "/home",
    name: "Home",
    component: Home,
  },

  {
    path: "/restaurant",
    name: "Restaurant",
    component: Restaurant,
  },
  {
    path: "/order",
    name: "Order",
    component: Order,
  },
  {
    path: "/menu",
    name: "Menu",
    component: Menu,
  },
  {
    path: "/menu/:productName",
    component: MenuCardWithMoreInfo,
  },
  {
    path: "/locations",
    name: "Locations",
    component: Locations,
  },
  {
    path: "/cart",
    name: "Cart",
    component: Cart,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior(to) {
    if (to.hash) {
      return {
        el: to.hash,
        behavior: "smooth",
      };
    }
  },
});

router.replace({ path: "/", redirect: "/home" });

export default router;
