import { createStore } from "vuex";
import menu from "./modules/menu";
import order from "./modules/order";
import cart from "./modules/cart";

export default createStore({
  modules: {
    menu,
    order,
    cart,
  },
});
