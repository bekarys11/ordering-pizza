import axios from "axios";

const cart = {
  state: {
    cart: [],
    successfulOrder: [],
    completedOrder: [],
  },
  mutations: {
    ADD_TO_CART(state, { product, quantity }) {
      let productAlreadyInCart = state.cart.find(
        (item) => item.product.id === product.id
      );
      if (productAlreadyInCart) {
        productAlreadyInCart.quantity += quantity;
        return;
      }

      state.cart.push({ product, quantity });
    },

    SET_ORDER(state, completedOrder) {
      state.completedOrder = completedOrder;
    },

    DELETE_FROM_CART(state, { product, quantity }) {
      let productAlreadyInCart = state.cart.find(
        (item) => item.product.id === product.id
      );
      if (productAlreadyInCart) {
        if (productAlreadyInCart.quantity === 0) {
          return;
        }
        productAlreadyInCart.quantity -= quantity;
        return;
      }

      state.cart.push({ product, quantity });
    },

    CLEAR_CART(state) {
      state.cart = [];
    },
  },

  actions: {
    async addProductToCart({ commit }, { product, quantity }) {
      commit("ADD_TO_CART", { product, quantity });
    },

    async postOrder({ commit }) {
      try {
        const response = await axios.post(
          "https://private-anon-5b1640d747-pizzaapp.apiary-mock.com/orders/",
          this.getters.orderedCart
        );

        commit("SET_ORDER", response.data);
      } catch (error) {
        console.log(error);
      }
    },
  },

  getters: {
    cart(state) {
      return state.cart;
    },

    cartItemCount(state) {
      return state.cart.length;
    },

    cartPriceTotal(state) {
      var total = 0;

      state.cart.forEach((item) => {
        total += item.product.price * item.quantity;
      });

      return total;
    },

    orderedCart(state) {
      let cart;
      state.cart.map((item) => {
        cart = [
          {
            menuItemId: item.product.id,
            quantity: item.quantity,
          },
        ];
      });
      return cart;
    },
  },
};

export default cart;
