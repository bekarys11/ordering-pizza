import axios from "axios";

const order = {
  state: {
    loading: false,
    restaurants: [],
  },
  actions: {
    async getRestaurants() {
      this.state.loading = true;
      const restaurants = await axios.get(
        "https://private-anon-06c97362d3-pizzaapp.apiary-mock.com/restaurants/"
      );
      this.state.restaurants = restaurants.data;
      this.state.loading = false;
    },
  },
};
export default order;
