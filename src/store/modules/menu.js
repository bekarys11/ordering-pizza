import axios from "axios";

const menu = {
  state: {
    menuData: [],
    isLoading: false,
    categories: [],
    names: [],
    pizzaOnly: [],
    drinkOnly: [],
    tillbehorOnly: [],
    hasToppings: false,
    toppings: "",
    restaurantID: null,
  },

  mutations: {
    DELETE_DUPLICATES() {
      let categories = [];
      let names = [];

      for (let i = 0; i < this.state.menuData.length; i++) {
        if (categories.indexOf(this.state.menuData[i].category) === -1) {
          categories.push(this.state.menuData[i].category);
        }

        if (names.indexOf(this.state.menuData[i].name) === -1) {
          names.push(this.state.menuData[i].name);
        }
      }
      this.state.categories = categories;
      this.state.names = names;
    },

    GET_PIZZA_ONLY() {
      let pizzaOnly = [];
      for (let i = 0; i < this.state.menuData.length; i++) {
        if (this.state.menuData[i].category === "Pizza")
          pizzaOnly.push(this.state.menuData[i]);
      }
      this.state.pizzaOnly = pizzaOnly;
    },

    GET_DRINKS_ONLY() {
      let drinkOnly = [];
      for (let i = 0; i < this.state.menuData.length; i++) {
        if (this.state.menuData[i].category === "Dryck")
          drinkOnly.push(this.state.menuData[i]);
      }
      this.state.drinkOnly = drinkOnly;
    },

    GET_TILLBEHOR_ONLY() {
      let tillbehorOnly = [];
      for (let i = 0; i < this.state.menuData.length; i++) {
        if (this.state.menuData[i].category === "Tillbehör")
          tillbehorOnly.push(this.state.menuData[i]);
      }
      this.state.tillbehorOnly = tillbehorOnly;
    },
  },

  actions: {
    async getMenu({ commit }) {
      try {
        this.state.isLoading = true;
        const menu = await axios.get(
          `https://private-anon-ac7a4454ee-pizzaapp.apiary-mock.com/restaurants/${this.state.restaurantID}/menu`
        );
        this.state.menuData = menu.data;
        commit("DELETE_DUPLICATES", this.state.menuData);
        this.state.isLoading = false;
      } catch (err) {
        console.log(err);
      }
    },

    async checkForToppingsLength(state, menu) {
      try {
        var toppings = 0;
        if (menu) {
          toppings = await menu.length;
        }
        state.hasToppings = toppings > 0 ? true : false;
      } catch (err) {
        console.log(err);
      }
    },
  },
  getters: {},
};

export default menu;
