## Table of contents

- [General info](#general-info)
- [Technologies](#technologies)

## General info

This project is an online pizza delivery website.

## Technologies

- Vue.js 3
- Sass

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
